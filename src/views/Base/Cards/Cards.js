import React, { Component } from 'react';
import Card from "../../../components/card/index";
import CardHeader from '../../../components/card/cardHeader';
import CardBody from '../../../components/card/cardBody';
import CardFooter from '../../../components/card/cardFooter';
import CardImage1 from '../../../assets/img/brand/card/Group 210.png';
import CardImage2 from '../../../assets/img/brand/card/Group 212.png';
import CardImage3 from '../../../assets/img/brand/card/Group 214.png';
import CardImage4 from '../../../assets/img/brand/card/Group 217.png';
import CardImage5 from '../../../assets/img/brand/card/Group 258.png';
import CardImage6 from '../../../assets/img/brand/card/Group 221.png';
import CardImage7 from '../../../assets/img/brand/card/Group 223.png';
import CardImage8 from '../../../assets/img/brand/card/Group 225.png';
import CardImage10 from '../../../assets/img/brand/card/Group 207.png';

class Cards extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300
    };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState } });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-12 col-sm-6 col-md-3">
            <Card>
              <CardHeader>
                <div className="Card-iconWrapper">
                  <img src={CardImage1} alt="Perfil"/>
                </div>
                <h2 className="Card-title">Editar Perfil</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-description">Gestiona la Información principal de tu cuenta. En ella encontrarás desde datos de facturación hasta impuestos o resumen de tu actividad en beonshop.</p>
              </CardBody>
              <CardFooter>
                <a href="#" className="Card-button">Gestionar <i class="fas fa-chevron-right"></i></a>
              </CardFooter>
            </Card>
          </div>
          <div className="col-12 col-sm-6 col-md-3">
            <Card>
              <CardHeader>
                <div className="Card-iconWrapper yellow">
                  <img src={CardImage2} alt="Formas de pago"/>
                </div>
                <h2 className="Card-title">Formas de pago</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-description">¿Tienes una, dos o incluso tres formas de pago activas? Gestiona con qué proveedor y cómo quieres cobrar a tus clientes.</p>
              </CardBody>
              <CardFooter>
                <a href="#" className="Card-button yellow">Gestionar <i class="fas fa-chevron-right"></i></a>
              </CardFooter>
            </Card>
         </div>
         <div className="col-12 col-sm-6 col-md-3">
            <Card>
              <CardHeader>
                <div className="Card-iconWrapper blue">
                  <img src={CardImage3} alt="Checkout"/>
                </div>
                <h2 className="Card-title">Checkout</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-description">Escoge como quieres que tus clientes realicen el proceso de compra, qué información rellenarán, qué será y qué no será obligatorio.</p>
              </CardBody>
              <CardFooter>
                <a href="#" className="Card-button blue">Gestionar <i class="fas fa-chevron-right"></i></a>
              </CardFooter>
            </Card>
         </div>
         <div className="col-12 col-sm-6 col-md-3">
            <Card>
              <CardHeader>
                <div className="Card-iconWrapper darkblue">
                  <img src={CardImage4} alt="Envios"/>
                </div>
                <h2 className="Card-title">Envios</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-description">Escoge con qué compañía o compañías quieres operar. Nosotros trabajamos con todas a través de Partners o con la que tú nos digas.</p>
              </CardBody>
              <CardFooter>
                <a href="#" className="Card-button darkblue">Gestionar <i class="fas fa-chevron-right"></i></a>
              </CardFooter>
            </Card>
          </div>
          <div className="col-12 col-sm-6 col-md-3">
              <Card>
                <CardHeader>
                  <div className="Card-iconWrapper">
                  <img src={CardImage5} alt="Emails"/>
                  </div>
                  <h2 className="Card-title">Emails</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-description">Edita el texto que tu tienda enviará a los usuarios y clientes para comunicarse con ellos. Ten en cuenta que es un importante canal de comunicación.</p>
                </CardBody>
                <CardFooter>
                  <a href="#" className="Card-button">Gestionar <i class="fas fa-chevron-right"></i></a>
                </CardFooter>
            </Card>
           </div>
           <div className="col-12 col-sm-6 col-md-3">
            <Card>
                <CardHeader>
                  <div className="Card-iconWrapper yellow">
                  <img src={CardImage6} alt="Canales de venta"/>
                  </div>
                  <h2 className="Card-title">Canales de venta</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-description padding">¿Tienes tiendas físicas u otros canales de venta? Edita aquí los puntos de venta que quieres mostrar en tu tienda. Si quieres además, los podemos conectar para que tengas un control total.</p>
                </CardBody>
                <CardFooter>
                  <a href="#" className="Card-button yellow">Gestionar <i class="fas fa-chevron-right"></i></a>
                </CardFooter>
            </Card>
          </div>
          <div className="col-12 col-sm-6 col-md-3">
            <Card>
                <CardHeader>
                  <div className="Card-iconWrapper blue">
                    <img src={CardImage7} alt="Facturación"/>
                  </div>
                  <h2 className="Card-title">Facturación</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-description">Encuentra todos los pedidos realizados por tus clientes, edita la factura tipo de tu tienda por si la solicita algún cliente.</p>
                </CardBody>
                <CardFooter>
                  <a href="#" className="Card-button blue">Gestionar <i class="fas fa-chevron-right"></i></a>
                </CardFooter>
            </Card>
         </div>
         <div className="col-12 col-sm-6 col-md-3">
            <Card>
                <CardHeader>
                  <div className="Card-iconWrapper darkblue">
                    <img src={CardImage8} alt="Planes y permisos"/>
                  </div>
                  <h2 className="Card-title">Planes y permisos</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-description">¿Quieres mejorar tu plan beonshop? ¿Cambiar permisos u otorgarlos a nuevos colaboradores? Todo eso lo podrás realizar aquí.</p>
                </CardBody>
                <CardFooter>
                  <a href="#" className="Card-button darkblue">Gestionar <i class="fas fa-chevron-right"></i></a>
                </CardFooter>
            </Card>
          </div>
          {/* <div className="col-12 col-sm-6 col-md-3">
              <Card>
                <CardHeader>
                  <div className="Card-iconWrapper">
                    
                  </div>
                  <h2 className="Card-title">Monedas</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-description">Edita las monedas con las que trabajará tu tienda y por tanto tus sistemas de pago. Ten en cuenta que existirán regarnos por transacción por parte de tu banco al realizar cambio de moneda.</p>
                </CardBody>
                <CardFooter>
                  <a href="#" className="Card-button">Gestionar <i class="fas fa-chevron-right"></i></a>
                </CardFooter>
            </Card>
           </div>
           <div className="col-12 col-sm-6 col-md-3">
            <Card>
                <CardHeader>
                  <div className="Card-iconWrapper yellow">
                    <img src={CardImage10} alt="Puntos"/>
                  </div>
                  <h2 className="Card-title">Puntos</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-description">Además de ayudarte con nuestros servicios, queremos otorgarte premios, descuentos y formación por el buen uso que hagas de la plataforma. Entra y gestiona tus puntos.</p>
                </CardBody>
                <CardFooter>
                  <a href="#" className="Card-button yellow">Gestionar <i class="fas fa-chevron-right"></i></a>
                </CardFooter>
            </Card>
           </div> */}
        </div>
      </div>
    );
  }
}

export default Cards;
