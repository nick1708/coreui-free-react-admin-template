import React, { Component } from 'react';
import Card from "../../../components/card/index";
import CardHeader from '../../../components/card/cardHeader';
import CardBody from '../../../components/card/cardBody';
import CardFooter from '../../../components/card/cardFooter';
import payMethodFile1 from '../../../assets//img/brand/payMethod/1-lacaixa.png';
import payMethodFile2 from '../../../assets//img/brand/payMethod/2-sabadell.png';
import payMethodFile3 from '../../../assets//img/brand/payMethod/3-BBVA.png';
import payMethodFile4 from '../../../assets//img/brand/payMethod/4-bankTransfer.png';
import payMethodFile5 from '../../../assets//img/brand/payMethod/5-bakinter.png';
import payMethodFile6 from '../../../assets//img/brand/payMethod/6-paypal.png';
import payMethodFile7 from '../../../assets//img/brand/payMethod/7-aplazame.png';

class PayMethod extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300
    };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState } });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <div className="row">
        <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <Card>
              <CardHeader>
                <img className="Card-payMethodImage" src={payMethodFile1} alt="La Caixa"/>
                <h2 className="Card-payMethodTitle">TPV La Caixa</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-payMethodText">El TPV de La Caixa se integra facilmente con su tienda virtual y ofrece una amplia gama de servicios para dar a sus clientes las máximas prestaciones.</p>
              </CardBody>
              <CardFooter>
                <div className="Card-footerWrapper">
                  <input id="chck" type="checkbox"></input>
                  <label for="chck" className="check-trail">
                    <span class="check-handler"></span>
                  </label>
                  <a href="#" className="Card-payMethodButton">Configurar</a>
                </div>
              </CardFooter>
            </Card>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <Card>
              <CardHeader>
                <img className="Card-payMethodImage" src={payMethodFile2} alt="Sabadell"/>
                <h2 className="Card-payMethodTitle">TPV Banc Sabadell</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-payMethodText">El TPV de La Caixa se integra facilmente con su tienda virtual y ofrece una amplia gama de servicios para dar a sus clientes las máximas prestaciones.</p>
              </CardBody>
              <CardFooter>
                <div className="Card-footerWrapper">
                  <input id="chck1" type="checkbox"></input>
                  <label for="chck1" className="check-trail">
                    <span class="check-handler"></span>
                  </label>
                  <a href="#" className="Card-payMethodButton">Configurar</a>
                </div>
              </CardFooter>
            </Card>
         </div>
         <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <Card>
              <CardHeader>
                <img className="Card-payMethodImage" src={payMethodFile3} alt="BBBVA"/>
                <h2 className="Card-payMethodTitle">TPV BBVA</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-payMethodText bvva">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra lectus tincidunt, hendrerit lacus eu, lorem sagittis eros.</p>
              </CardBody>
              <CardFooter>
                <div className="Card-footerWrapper">
                  <input id="chck2" type="checkbox"></input>
                  <label for="chck2" className="check-trail">
                    <span class="check-handler"></span>
                  </label>
                  <a href="#" className="Card-payMethodButton">Configurar</a>
                </div>
              </CardFooter>
            </Card>
         </div>
         <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <Card>
              <CardHeader>
                <img className="Card-payMethodImage" src={payMethodFile4} alt="TransferBank"/>
                <h2 className="Card-payMethodTitle">Transferencia bancaria</h2>
              </CardHeader>
              <CardBody>
                <p className="Card-payMethodText bvva">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra lectus tincidunt, hendrerit lacus eu, lorem sagittis eros.</p>
              </CardBody>
              <CardFooter>
              <div className="Card-footerWrapper">
                  <input id="chck3" type="checkbox"></input>
                  <label for="chck3" className="check-trail">
                    <span class="check-handler"></span>
                  </label>
                  <a href="#" className="Card-payMethodButton">Configurar</a>
                </div>
              </CardFooter>
            </Card>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">
              <Card>
                <CardHeader>
                  <img className="Card-payMethodImage" src={payMethodFile5} alt="Bakinter"/>
                  <h2 className="Card-payMethodTitle">TPV Bankinter</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-payMethodText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra lectus tincidunt, hendrerit lacus eu, lorem sagittis eros.</p>
                </CardBody>
                <CardFooter>
                  <div className="Card-footerWrapper">
                    <input id="chck4" type="checkbox"></input>
                    <label for="chck4" className="check-trail">
                      <span class="check-handler"></span>
                    </label>
                    <a href="#" className="Card-payMethodButton">Configurar</a>
                  </div>
                </CardFooter>
            </Card>
           </div>
           <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <Card>
                <CardHeader>
                <img className="Card-payMethodImage" src={payMethodFile6} alt="Paypal"/>
                <h2 className="Card-payMethodTitle">Paypal</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-payMethodText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra lectus tincidunt, hendrerit lacus eu, lorem sagittis eros.</p>
                </CardBody>
                <CardFooter>
                  <div className="Card-footerWrapper">
                    <input id="chck5" type="checkbox"></input>
                    <label for="chck5" className="check-trail">
                      <span class="check-handler"></span>
                    </label>
                    <a href="#" className="Card-payMethodButton">Configurar</a>
                  </div>
                </CardFooter>
            </Card>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <Card>
                <CardHeader>
                  <img className="Card-payMethodImage" src={payMethodFile7} alt="Aplazame"/>
                  <h2 className="Card-payMethodTitle">Aplazame</h2>
                </CardHeader>
                <CardBody>
                  <p className="Card-payMethodText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra lectus tincidunt, hendrerit lacus eu, lorem sagittis eros.</p>
                </CardBody>
                <CardFooter>
                  <div className="Card-footerWrapper">
                      <input id="chck6" type="checkbox"></input>
                      <label for="chck6" className="check-trail">
                        <span class="check-handler"></span>
                      </label>
                      <a href="#" className="Card-payMethodButton">Configurar</a>
                    </div>
                </CardFooter>
            </Card>
         </div>
        </div>
      </div>
    );
  }
}

export default PayMethod;
