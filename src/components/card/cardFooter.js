import React from 'react';
import PropTypes from 'prop-types';

const CardFooter = (props) => (
    <div className="CardFooter">
        {props.children}
    </div>
);

export default CardFooter;