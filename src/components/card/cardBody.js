import React from 'react';
import PropTypes from 'prop-types';

const CardBody = (props) => (
    <div className="CardBody">
        {props.children}
    </div>
);

export default CardBody;