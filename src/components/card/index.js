import React from 'react';
import PropTypes from 'prop-types';

export default function Card(props) {
    return (
        <div className="Card">
            {props.children}
        </div>
    )
}

Card.propTypes = {
    cardClass: PropTypes.string  
}