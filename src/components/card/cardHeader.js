import React from 'react';
import PropTypes from 'prop-types';

const CardHeader = (props) => (
    <div className="CardHeader">
        {props.children}
    </div>
);

export default CardHeader;